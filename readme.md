# Google API Push Notifications Debug

## Requirements

* PHP >= 5.6.4
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension

## Setup

1. Create a new Google Project at https://console.developers.google.com
2. Enable Google Drive API
3. Create Service Account Key (Credentials tab)
4. Make sure Enable G Suite Domain-wide Delegation option is enabled in Service Account Key options (oAuth Consent Screen Configuration is required for this)
5. **Copy the downloaded credentials file into PROJECT_DIR/.auth-config file**
6. Set the project URL in .env file (copy .env.example) and make sure the domain is inside the allowed  domains list in your Google Project panel.
7. Install PHP Composer and run composer update inside the PROJECT_DIR.