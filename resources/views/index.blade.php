<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap Login &amp; Register Templates</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/form-elements.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Top content -->
<div class="top-content">
    <div class="container">

        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 text">
                <h1>Google Push Notifications Debug</h1>
                <span>
                    Client name: <strong>{{ $client }}</strong><br />
                    Scopes: <strong>{{ $scopes }}</strong>
                </span>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 col-sm-offset-1 show-forms">
                <span class="show-register-form active">Browse</span>
                <span class="show-login-form">existing channels</span>
            </div>
            <div class="col-sm-4 show-forms">
                <span class="show-register-form active">Create</span>
                <span class="show-login-form">a new channel</span>
            </div>
        </div>

        <div class="row ">
            <div class="col-sm-6 col-sm-offset-1 forms-right-icons">
                <div class="row">
                    @foreach($channels as $channel)
                        <div class="col-sm-12">
                            <a href="./{{ $channel }}">{{ $channel }}</a>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-sm-4">
                <form role="form" action="./" method="post" class="r-form">
                    <div class="form-group">
                        <label class="sr-only" for="r-form-first-name">Channel ID</label>
                        <input type="text" name="id" placeholder="Channel ID..." class="r-form-first-name form-control" id="r-form-first-name">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="r-form-email">Email</label>
                        <input type="text" name="email" placeholder="Email..." class="r-form-email form-control" id="r-form-email">
                    </div>
                    <button type="submit" class="btn">Create</button>
                </form>
            </div>
        </div>

    </div>
</div>

</body>

</html>