<?php

/**
 * Application routes
 * @see \App\Http\Controllers\ChannelController
 */

$app->get('/', 'ChannelController@index');
$app->post('/', 'ChannelController@store');
$app->post('/receive', 'ChannelController@receive');
$app->get('/{id}', 'ChannelController@show');