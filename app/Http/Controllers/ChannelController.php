<?php

namespace App\Http\Controllers;

use App\Channel;
use Carbon\Carbon;
use Flintstone\Flintstone;
use Google_Client;
use Google_Service_Drive;
use Google_Service_Drive_Channel;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Exception;

/**
 * Class ChannelController
 * @package App\Http\Controllers
 */
class ChannelController extends Controller
{
    /**
     * @var Google_Service_Drive
     */
    protected $drive;

    /**
     * @var Flintstone
     */
    protected $channels;

    /**
     * @var Flintstone
     */
    protected $notifications;

    /**
     * Create a new controller instance.
     * @param Google_Client $client
     * @return void
     */
    public function __construct(Google_Client $client)
    {
        $this->drive = new Google_Service_Drive($client);

        $config = [
            'dir' => storage_path('db')
        ];
        $this->channels = new Flintstone('channels', $config);
        $this->notifications = new Flintstone('notifications', $config);
    }

    /**
     * Index existing channels
     */
    public function index()
    {
        $authConfigJson = file_get_contents(base_path('.auth-config'));
        $authConfig = json_decode($authConfigJson);

        return view('index')
            ->with('client', $authConfig->client_id)
            ->with('scopes', 'https://www.googleapis.com/auth/drive')
            ->with('channels', $this->channels->getKeys());
    }

    /**
     * Create a new channel
     * @param Request $request
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|alpha_num',
            'email' => 'required|email'
        ]);

        $this->drive->getClient()->setSubject($request->input('email'));
        $id = $request->input('id');
        $address = env('APP_URL') . '/receive';

        $channel = new Google_Service_Drive_Channel();
        $channel->setId($id);
        $channel->setType('web_hook');
        $channel->setAddress($address);
        $channel->setExpiration((time() + 7 * 24 * 60 * 60) * 1000); // now + 7 days
        $channel->setToken(str_random(64));

        // todo handle exception
        $channel = $this->drive->changes->watch(
            $this->drive->changes->getStartPageToken()->getStartPageToken(),
            $channel
        );

        $this->channels->set($channel->getId(), [
            'resource_id' => $channel->getResourceId(),
            'resource_uri' => $channel->getResourceUri(),
            'address' => $channel->getAddress(),
            'token' => $channel->getToken(),
            'created_at' => Carbon::now()->getTimestamp(),
            'expires_at' => substr($channel->getExpiration(), 0, -3)
        ]);

        return redirect('/');
    }

    /**
     * Receive a channel push notification
     * @param Request $request
     * @return JsonResponse
     */
    public function receive(Request $request)
    {
        $channelId = $request->header('X-Goog-Channel-ID');
        $token = $request->header('X-Goog-Channel-Token');

        if ( ! $channelId || ! $token)
        {
            return new JsonResponse(null, 400);
        }

        $channel = $this->channels->get($channelId);

        if (empty($channel) || $channel['token'] !== $token)
        {
            return new JsonResponse(null, 400);
        }

        $notifications = $this->notifications->get($channelId);

        if (empty($notifications)) $notifications = [];

        $now = time();

        $notifications[] = [
            'created_at' => $now,
            'headers' => $request->headers->all(),
            'data' => $request->all()
        ];

        $this->notifications->set($channelId, $notifications);

        $channel['updated_at'] = $now;

        $this->channels->set($channelId, $channel);

        return new JsonResponse();
    }

    /**
     * Show specific channel
     * @param string $id
     */
    public function show($id)
    {
        try {
            $channel = $this->channels->get($id);
        }
        catch(Exception $e)
        {
            return new JsonResponse(null, 404);
        }

        $createdAt = Carbon::createFromTimestamp($channel['created_at']);
        $expiresAt = Carbon::createFromTimestamp($channel['expires_at']);
        $updatedAt = array_has($channel, 'updated_at') ? Carbon::createFromTimestamp($channel['updated_at']) : null;

        $channel['created_at'] = $createdAt->toDateTimeString();
        $channel['expires_at'] = $expiresAt->toDateTimeString();
        $channel['updated_at'] = $updatedAt ? $updatedAt->toDateTimeString() : 'never';
        $channel['lifetime_minutes'] = $createdAt->diffInMinutes($expiresAt);
        $channel['activity_minutes'] = $updatedAt ? $createdAt->diffInMinutes($updatedAt) : 0;
        unset($channel['token']);

        $notifications = $this->notifications->get($id);

        if (empty($notifications)) $notifications = [];

        foreach ($notifications as $key => $notification)
        {
            $notifications[$key]['created_at'] = Carbon::createFromTimestamp($notification['created_at'])->toDateTimeString();
        }

        return view('show')
            ->with('id', $id)
            ->with('channel', $channel)
            ->with('notifications', $notifications);
    }
}
