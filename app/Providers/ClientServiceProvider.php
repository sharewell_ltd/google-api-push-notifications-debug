<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Google_Client;
use Exception;
use Google_Service_Drive;

class ClientServiceProvider extends ServiceProvider
{
    /**
     * Register client service.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Google_Client', function($app)
        {
            $client = new Google_Client();

            $authConfigPath = base_path('.auth-config');
            
            if ( ! file_exists($authConfigPath))
            {
                throw new Exception(
                    'Please make sure to move obtained service account credentials file into ".auth-config" file inside project dir.'
                );
            }

            $client->setAuthConfig($authConfigPath);
            $client->addScope(Google_Service_Drive::DRIVE);

            return $client;
        });
    }
}
